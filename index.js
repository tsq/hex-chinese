module.exports = {
	toHex: function (chinese) {
		var buf = new Buffer(chinese);
		var result = '';
		for (var i = 0; i < buf.length; i++) {
		  result += buf[i].toString(16);
		}
	  return result;
	},
	toChinese: function (hex) {
		if (typeof hex !== 'string') {
		  throw new Error('hex expect a string');
		}
	  return new Buffer(hex, 'hex').toString();
	}
};
