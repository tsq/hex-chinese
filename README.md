# Hex <=> Chinese [![Build Status](https://travis-ci.org/tsq/hex-chinese.svg?branch=master)](https://travis-ci.org/tsq/hex-chinese)

## Demo
```javascript
var test = require("./");
var assert = require("assert");

var cn = test.toChinese('E4B880');
var hex = test.toHex('一');
assert.equal(cn, '一');
assert.equal(hex, 'E4B880'.toLowerCase());
```
