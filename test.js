'use strict';
var test = require("./");
var assert = require("assert");

var cn = test.toChinese('E4B880');
var hex = test.toHex('一');
assert.equal(cn, '一');
assert.equal(hex, 'E4B880'.toLowerCase());
